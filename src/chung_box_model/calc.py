# -*- coding: utf-8 -*-
r"""
calc.py

Calculating functions for the box model.

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20180710    EC  Initial code. Branched from routines.py.
20200822    EC  Packaged.
"""
# Standard Library imports
import numpy as np
import pandas as pd
import re


def mass_box(
        mass=5.1170001e+21,
        distribution=np.repeat([0.125, 0.075, 0.050], 4)
        ):
    r"""Calculate mass in each box

    Parameters
    ----------
    mass : float
        Total mass to be distributed.
    distribution : ndarray.
        Fractions.

    Returns
    -------
    Distributed mass

    """
    return mass*distribution


def delta(isotopologue, isotopologue_table, chi, mode='normal'):
    r"""Calculate delta using standards

    Parameters
    ----------
    isotopologue : str
        Isotopologue.
    isotopologue_table : DataFrame
        Table containing properties of different isotopologues.
    chi : dict
        Molar mixing ratio of selected and reference isotopologues.
        {isotopologue : ndarray}
    mode : {'normal', 'big'}, optional
        Lowercase or uppsecase delta notation.

    Returns
    -------
    delta : ndarray

    """
    if 'C12H4' in chi:
        chi_sel = chi[isotopologue]
        chi_C12H4 = chi['C12H4']
        if mode == 'big':
            # ratio = R_stochastic
            chi_C13H4 = chi['C13H4']
            chi_C12H3D = chi['C12H3D']
            # C13 = chi_C13H4/(chi_C12H4 + chi_C13H4)
            # C12 = chi_C12H4/(chi_C12H4 + chi_C13H4)
            # H = 4*chi_C12H4/(4*chi_C12H4 + chi_C12H3D)
            # D = chi_C12H3D/(4*chi_C12H4 + chi_C12H3D)
            if isotopologue == 'C13H3D':
                # ratio = C13*D*H**3*4/C12*H**4
                ratio = chi_C13H4*chi_C12H3D/chi_C12H4**2
            if isotopologue == 'C12H2D2':
                # ratio = C12*H**2*D**2*6/C12*H**4
                ratio = 3.0/8.0*chi_C12H3D**2/chi_C12H4**2
        else:
            # ratio = R_std
            ratio = isotopologue_table['R_std'][isotopologue]
        delta = ((chi_sel/chi_C12H4)/ratio - 1)*1.0e3
    return delta


def delta2frac(delta, iso_table, mode='n'):
    r"""Small delta -> molar/mass fraction.

    The delta notation for methane is:
    delta_i = (R_{i,sample}/R_{i,std} - 1)*1e3
    where R_i = [i]/[C12H4]

    Hence:
    R_{i,sample} = (delta_i*1e-3 + 1)*R_std

    Then:
    mR_i = m_i / m_C12H4
         = R_i * M_i / M_C12H4
    mR_{i,CH4} = m_i / m_CH4
               = m_i / m_CH4 * m_C12H4 / m_C12H4
               = mR_i / mR_C12H4
               = mR_i / (\sum_{i} mR_i)

    Caution: the sum only include isotopes/isotopologues defined in the input
    table, but would be a reasonable approximation.

    Parameters
    ----------
    delta : DataFrame
        Small delta values of isotopes/isotopologues.
    iso_table : DataFrame
        Table containing properties of different isotopes/isotopologues.
    mode : str {'n', 'm'}
        'n' for mole fraction, 'm' for mass fraction

    Returns
    -------
    mR : ndarray
        Mass ratios.

    """
    # if isinstance(delta, pd.DataFrame)
    R_std = iso_table['R_std']
    R_sample = (delta*1.e-3 + 1.).mul(R_std, axis=0)
    if mode == 'n':
        fraction = R_sample / R_sample.sum(axis=0)
    else:
        mol_m = iso_table['mol_m']
        if 'C12H4' in iso_table.index:
            iso_common = 'C12H4' 
        mR_sample = R_sample.mul(mol_m/mol_m[iso_common], axis=0)
        fraction = mR_sample / mR_sample.sum(axis=0)
    return fraction


def Delta2delta_CH4(idata, Delta, R_std):
    r"""Convert Delta to delta for CH4

    Parameters
    ----------
    idata : ndarray
        Array containing isotopic signature. 
    Delta : array-like
        The first dimension should be 2, for commonest clumped isotopologues.
    R_std : array-like
        R_std.

    Returns
    -------
    delta : ndarray
        Isotopic signatures including non-clumped.
    """
    R = (idata*1.e-3 + 1) * R_std
    R[..., 3, :] = (Delta[..., 0, :]*1.e-3 + 1.) * R[..., 1, :] * R[..., 2, :]
    R[..., 4, :] = (Delta[..., 1, :]*1.e-3 + 1.) * 3./8. * R[..., 2, :]**2
    delta = R/R_std*1.e3 - 1.e3
    return delta


def delta2RM(delta, R_std, M):
    r"""Convert delta to mass ratio

    Parameters
    ----------
    delta : dict
        Isotopic signatures.
    R_std : array-like
        R_std.
    M : array-like
        Molecular mass.

    Returns
    -------
    RM : ndarray
        Mass ratio (m_i/m_i0).

    """
    RM = {k: (v*1.e-3 + 1.) * R_std * M for k, v in delta.items()}
    return RM


def q_iso(q, RM, iso_sel=[]):
    r"""Calculate mass of isotopologues in emissions.

    Parameters
    ----------
    q : dict
        {variable: ndarray}
        Emissions.
    RM : dict
        {variable: ndarray}
        Mass ratios (m_i/m_i0) in emissions.
    iso_sel : list of int
        Isotope/isotopologues to include in the summation.

    Returns
    -------
    q_i : ndarray
        Emissions of each isotopologues.

    """
    sectors = [re.sub('^q_', '', k) for k in q]
    if not iso_sel:
        iso_sel = np.s_[:]
    q_i = sum([
        q['q_{}'.format(k)]
        * RM['dq_{}'.format(k)]
        / RM['dq_{}'.format(k)][..., iso_sel, :].sum(-2, keepdims=True)
        for k in sectors
        ])
    return q_i


def combine_dq(q, dq, R_std, M, iso_sel=[]):
    r"""Calculate total source isotopic signature

    Parameters
    ----------
    q : dict
        {variable: ndarray}
        Emissions.
    dq : dict
        {variable: ndarray}
        Isotopic signatures of emissions.
    R_std : array-like
        R_std.
    M : array-like
        Molecular mass.
    iso_sel = list of int
        Isotope/Isotopologues to include.

    Returns
    -------
    dq_total : ndarray
        Combined isotopic signature.

    """
    sectors = [re.sub('^q_', '', k) for k in q]
    RM = delta2RM(dq, R_std, M)
    if not iso_sel:
        iso_sel = np.s_[:]
    q_ni = q_iso(q, RM, iso_sel) / M
    dq_total = (q_ni / q_ni[..., :1, :] / R_std - 1.) * 1.e3
    return dq_total
    

def cor(idata, dim_data, keys):
    r"""Calculate (posterior) correlation matrix
    
    Parameters
    ----------
    idata : dict
        {variable: ndarray}
        Input data.
    dim_data : dict
        {variable: tuple}
        Input data size.
    keys : list of str
        variables to include.

    Returns
    -------
    d : ndarray
        n x n correlation matrix.

    """
    d = np.corrcoef(
            np.concatenate(
                [
                    idata[k].reshape(-1, np.prod(v))
                    for k, v in dim_data.items()
                    if k in keys
                    ],
                1
                ).T
            )
    return d

