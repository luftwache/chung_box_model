# -*- coding: utf-8 -*-
r"""
iso_model.py

Module containing Model class

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20180710    EC  Initial code.
20200822    EC  Packaged.
"""
# Standard Library imports
import numpy as np
import re

# Local imports
import chung_box_model.forward as a_forward


class Model:
    def __init__(
            self, iso, iso_table,
            c0=None, d0=None,
            q={}, dq={}, m_atm=None,
            OH=None, OH_a=0.0, Cl=None, Cl_a=None, T=None,
            tau=None, KIE_tau=1.0, KIE_tau_factor=1.,
            ib=None, ti=None, vi=None, tv=None, vv=None, dt=172800
            ):
        r"""Wrapper to run forward model for different isotopologues

        All parameters defaulting to None and marked optional other than tv
        and vv should be updated to run the model.

        Parameters
        ----------
        iso : list
            List of str
            Isotopologues to include in the model.
        iso_table : pd.DataFrame
            Table containing isotopologue properties.
        c0 : ndarray, optional
            1d, n_box
            Initial mixing ratios of each boxes (unit: nmol mol^-1 ~ppbv).
        d0 : ndarray, optional
            2d, len(iso) x n_box
            Initial delta values (unit: per mil).
        q : dict, optional
            {sector: 2darray (n_months x n_box_surface)}
            Sectorly emissions (unit: Tg a^-1).
        dq : dict, optional
            {sector: 1|3darray (len(iso) (x n_months x n_box))}
            Sectorly delta values.
        m_atm : ndarray, optional
            1d, n_box
            Air mass of individual boxes (unit: g).
        OH, Cl, T : ndarray, optional
            2d, month x n_box
            Concentrations of sink reactants (unit: 10^6 molec cm^-3),
            Temperature (unit: K),
        tau : ndarray, optional
            4d, month, sinks, 1, n_box
            Lifetime due to other unspecified losses (unit: a).
        KIE_tau : ndarray, optional
            3d, sinks, len(iso), 1
            KIE of sinks in tau
        KIE_tau_factor : ndarray, optional
            3d, sinks, 1, 1
            factor muliplied for KIE_tau
        ib : ndarray, optional
            3d, month x n_box x n_box
            Transport parameter matrix.
        dt : float, optional
            Delta time (s).

        To Do
        -----
        arr, conc to unified arrays arr_chem and chem
        """
        self.iso = iso
        self.iso_table = iso_table
        self.c0 = c0
        self.d0 = d0
        self.q = q
        self.dq = dq
        self.m_atm = m_atm
#        self.chem = chem
        self.OH = OH
        self.OH_a = OH_a
        self.Cl = Cl
        self.T = T
        self.tau = tau
        self.KIE_tau = KIE_tau
        self.KIE_tau_factor = KIE_tau_factor
        self.ib = ib
        self.ti = ti
        self.vi = vi
        self.ti = tv
        self.vi = vv
        self.dt = dt
        self.mol_m = np.array([[iso_table['mol_m'][i]] for i in iso])
        self.R_std = np.array([[iso_table['R_std'][i]] for i in iso])

    def calc_q(self, iso_sel=[]):
        r"""Calculate emissions of each isotopologues

        Parameters
        ----------
        iso_sel : list-like
            Isotopologues to be included in the total.

        Returns
        -------
        q_i : ndarray
            3d, n_month x n_iso x n_box

        """
        q = self.q
        sectors = [re.sub('^q_', '', k) for k in q]
        mR = {
                k:
                ((self.dq['dq_{}'.format(k)]*1.e-3 + 1.)
                    * self.R_std * self.mol_m)
                for k in sectors
                }
        if iso_sel:
            q_i = sum([
                q['q_{}'.format(k)]
                * mR[k]
                / mR[k][..., iso_sel, :].sum(axis=-2, keepdims=True)
                for k in sectors
                ])
        else:
            q_i = sum([
                q['q_{}'.format(k)]
                * mR[k]
                / mR[k].sum(axis=-2, keepdims=True)
                for k in sectors
                ])
        return q_i

    def run(self, iso_sel=[]):
        r"""Run the forward model
        
        Parameters
        ----------
        iso_sel : list-like
            Isotopologues to be included in the total.

        Returns
        -------
        odata : OrderedDict
            Contains mixing ratio, burden, emissions, losses, sink lifetimes.

        """
        odata = {}
        q = self.calc_q(iso_sel=iso_sel)
        R0 = (self.d0*1.e-3 + 1.0)*self.R_std
        if not iso_sel:
            c0 = self.c0 * R0 / R0.sum(axis=0)
        else:
            c0 = self.c0 * R0 / R0[..., iso_sel, :].sum(axis=0)

        n_chem = 1
        n_tau = self.tau.shape[1]
        loss_factor = np.zeros((
            self.T.shape[0],
            n_chem + n_tau,
            len(self.iso),
            self.m_atm.size
            ))
        A_OH = self.iso_table['A_OH']
        ER_OH = self.iso_table['E/R_OH']
        arr_A = np.array([A_OH[i] for i in self.iso])[np.newaxis, :]
        arr_ER = np.array([ER_OH[i] for i in self.iso])[np.newaxis, :]
        # A_Cl = self.iso_table['A_Cl']
        # ER_Cl = self.iso_table['E/R_Cl']
        # arr_A = np.array([[A_OH[i], A_Cl[i]] for i in self.iso]
        #                  ).swapaxes(0, 1)
        # arr_ER = np.array([[ER_OH[i], ER_Cl[i]] for i in self.iso]
        #                   ).swapaxes(0, 1)
        loss_factor[:, :n_chem] = np.einsum(
                'jk,jkil,ijl->ijkl',
                arr_A,
                np.exp(-arr_ER[..., np.newaxis, np.newaxis]/self.T),
                np.array([self.OH*(1.+self.OH_a)]).swapaxes(0, 1) * 1.e6
                )
        loss_factor[:, -n_tau:] = (
                1. / self.tau
                / ((self.KIE_tau - 1) * self.KIE_tau_factor + 1.)
                / 365.25 / 24.0 / 3600.0
                )

        c_mon, burden_mon, q_mon, loss_mon, tau_mon = (
            a_forward.run_model(
                c0=c0,
                q=q,
                mol_m=self.mol_m,
                m_atm=self.m_atm,
                loss_factor=loss_factor,
                ib=self.ib,
                dt=self.dt
                )
             )
        odata = {'c': c_mon, 'burden': burden_mon, 'q': q_mon,
                 'loss': loss_mon, 'tau': tau_mon}
        return odata

