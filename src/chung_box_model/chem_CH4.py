# -*- coding: utf-8 -*-
r"""
chem_CH4.py

Script to produce table of isotopologue properties for methane.

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20171026    EC  Initial code.
20200822    EC  Packaged.
"""
# Standard Library imports
import numpy as np
import pandas as pd
from scipy.special import factorial


# =============================================================================
# Values
#
# For OH + CH4 -> CH3 + H20 reaction, A = 2.45×10e–12 and E/R = 1775
# Sander, S. P. (2006), Chemical kinetics and photochemical data for use in
# atmospheric studies, Evaluation 15 JPL Publ. No. 6–2, 523 pp. page 1-11
# =============================================================================
isotopes = \
    pd.DataFrame({'C12': [12.000000, 1.0],
                  'C13': [13.003355, 1.12372e-2],
                  'H': [1.007825, 1.0],
                  'D': [2.014102, 1.5576e-4]},
                 index=['mol_m', 'delta_0'])

contents = \
    pd.DataFrame({'C12H4': [1, 0, 4, 0],
                  'C13H4': [0, 1, 4, 0],
                  'C12H3D': [1, 0, 3, 1],
                  'C13H3D': [0, 1, 3, 1],
                  'C12H2D2': [1, 0, 2, 2]},
                 index=['C12', 'C13', 'H', 'D'])

KIE = \
    pd.DataFrame({'C12H4': [1.0, 1.0],
                  'C13H4': [1.0060, 1.0280],
                  'C12H3D': [1.3208, 1.4100],
                  'C13H3D': [1.3292, 1.4500],
                  'C12H2D2': [1.9066, 2.1840]},
                 index=['KIE_OH', 'KIE_Cl'])

rate = \
    pd.DataFrame({'OH': [2.45e-12, 1775],
                  'Cl': [7.1e-12, 1270]},
                 index=['Arrhenius_A', 'Arrhenius_E/R'])

# =============================================================================
# Table Construction
# =============================================================================
isotopologues = contents.columns
CH4_table = pd.DataFrame({}, columns=isotopologues)

isotopes.loc['NA'] = {'C12': [1.0/(isotopes['C13']['delta_0'] + 1.0)],
                      'C13': [isotopes['C13']['delta_0']/
                              (isotopes['C13']['delta_0'] + 1.0)],
                      'H': [1.0/(isotopes['D']['delta_0'] + 1.0)],
                      'D': [isotopes['D']['delta_0']/
                            (isotopes['D']['delta_0'] + 1.0)]}

# Molecular Mass
mol_m = pd.Series(np.zeros(len(isotopologues)), index=isotopologues)
for iso in isotopologues:
    for i in isotopes.columns:
        mol_m[iso] += isotopes[i]['mol_m']*contents[iso][i]
CH4_table.loc['mol_m'] = mol_m

# Absolute fractional abundance
NA = pd.Series(np.ones(len(isotopologues)), index=isotopologues)
for iso in isotopologues:
    for i in isotopes.columns:
        if i in ['C12', 'C13']:
            n = contents[iso][['C12', 'C13']].sum()
        elif i in ['H', 'D']:
            n = contents[iso][['H', 'D']].sum()
        r = contents[iso][i]
        if i in ['C12', 'H']:  # Calculate combination once
            combination = factorial(n)/factorial(r)/factorial(n - r)
        else:
            combination = 1.0
        NA[iso] *= combination*isotopes[i]['NA']**contents[iso][i]
CH4_table.loc['NA'] = NA

# Relative abundance = abundance / (abundance of CH4) in reference gas
R_std = NA/NA['C12H4']
CH4_table.loc['R_std'] = R_std

# rate
CH4_table.loc['KIE_OH'] = KIE.loc['KIE_OH']
CH4_table.loc['A_OH'] = rate['OH']['Arrhenius_A']/KIE.loc['KIE_OH']
CH4_table.loc['E/R_OH'] = rate['OH']['Arrhenius_E/R']
CH4_table.loc['KIE_Cl'] = KIE.loc['KIE_Cl']
CH4_table.loc['A_Cl'] = rate['Cl']['Arrhenius_A']/KIE.loc['KIE_Cl']
CH4_table.loc['E/R_Cl'] = rate['Cl']['Arrhenius_E/R']

CH4_table = CH4_table.transpose()

