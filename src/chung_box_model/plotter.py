# -*- coding: utf-8 -*-
r"""
plotter.py

Plotting tools

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20180710    EC  Initial code.
20200822    EC  Packaged.
"""

# Standard library imports
import matplotlib.pyplot as plt
import numpy as np


def tseries_line(
        time, variables, colours, columns,
        dimension=[4, 3, 2, 2, 0.15, 0.01, 0.01, 0.1, 0.01, 8],
        ylim=[None, None],
        uncertainties={},
        share_axes=True,
        legend=False,
        ylabel=None, force_ylim=False, auto_ymargin=True,
        hline=None,
        ofile=None
        ):
    r"""Line plots from multiple data in a grid.

    Parameters
    ----------
    time : ndarray
        1d array containing decimal years.
    variables : dict
        Contains {label: data}.
    colours : dict
        Contains {label: colour}.
    columns : list of int
        Columns of from the data to use.
    dimension : list of float
        Dimension of the plot. It should contain 8 float for:
            figure size: height, width
            figure layout: rows, columns
            margins: left, right, top, bottom
    ylim : tuple or list
        Y axes limits.
    uncertainties : dict
        Uncertainty range of variables.
        Contains {label: [min, max]}.
    share_axes : boolean (default True)
        If True, Y axis limit on all panels will be the same.
    legend : boolean
        If True, draw legend.
    ylabel : str, optional
        Label for Y axis.
    force_ylim : boolean
        If false, the ylim will adjust to the data.
    auto_ymargin : boolean
        If True, there will be extra space between Y axis limits and top/bottom
        margins.
    ofile : path-like
        Output file for the image. If None, print to screen.

    """
    valid = [d for d in [variables, uncertainties] if d and d is not None]
    if (np.array([v.shape[-1] for v in variables.values()]) == 1).any():
        columns = [0]
    # Set ylim
    min_y = np.nanmin([np.amin(np.ma.masked_invalid(np.array(v)[..., columns]))
                       for d in valid
                       for v in d.values()])
    max_y = np.nanmax([np.amax(np.ma.masked_invalid(np.array(v)[..., columns]))
                       for d in valid
                       for v in d.values()])
    if (ylim[0] is not None):
        if force_ylim:
            ymin = ylim[0]
        else:
            ymin = np.amin([ylim[0], min_y])
    else:
        ymin = min_y
    if (ylim[1] is not None):
        if force_ylim:
            ymax = ylim[1]
        else:
            ymax = np.amax([ylim[1], max_y])
    else:
        ymax = max_y

    if auto_ymargin and not force_ylim:
        y_range = ymax - ymin
        log_y_range = np.floor(np.log10(y_range))
        factor = 10**(-log_y_range)
        ymin = (np.floor(ymin * factor)) / factor
        ymax = (np.ceil(ymax * factor)) / factor

    # =========================================================================
    #    Plot
    # =========================================================================
    fontsize = dimension[9]
    n_rows = dimension[2]
    n_cols = dimension[3]
    fig = plt.figure(figsize=(dimension[0], dimension[1]), dpi=300)
    w = (1. - dimension[4] - dimension[5] - dimension[8]*(n_cols-1)) / n_cols
    h = (1. - dimension[6] - dimension[7] - dimension[8]*(n_rows-1)) / n_rows

    # Set xlim
    n_years = np.floor(time[-1] - time[0]) + 1
    if n_years <= 10:
        tick_space = 1*n_rows
    elif n_years <= 20:
        tick_space = 2*n_rows
    elif n_years <= 50:
        tick_space = 5*n_rows
    elif n_years <= 100:
        tick_space = 10*n_rows
    elif n_years <= 500:
        tick_space = 50*n_rows
    elif n_years <=1000:
        tick_space = 100
    else:
        tick_space = 200
    major_locator = np.arange(time[0],
                              time[0] + n_years + tick_space,
                              tick_space*dimension[2])

    # background for axis labels
    ax = fig.add_axes([0.06,
                       0.06,
                       1. - 0.06 - dimension[5],
                       1. - dimension[6] - 0.06])
    ax.set_frame_on(False)
    ax.tick_params(
        axis='both',
        which='both',
        bottom=False, top=False, left=False, right=False,
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)
    #ax.set_xlabel('year', fontsize=fontsize)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=fontsize)

    # plots
    y_range = ymax - ymin
    t_range = time[-1] - time[0]
    for i in columns:
        col_no = i % n_cols
        row_no = i // n_rows
        ax = fig.add_axes([dimension[4] + col_no * (w + dimension[8]),
                           1 - dimension[6] - row_no * (h + dimension[8]) - h,
                           w,
                           h])
        #ax.set_ylim(ymin - y_range*0.1, ymax + y_range*0.1)
        #ax.set_xlim(time[0] - t_range*0.1, time[-1] + t_range*0.1)
        ax.set_ylim(ymin, ymax)
        ax.set_xlim(time[0], time[-1])
        ax.xaxis.set_major_locator(plt.FixedLocator(major_locator))
        # tick label appearance
        if share_axes:
            if (col_no == 0) and (row_no == n_rows - 1):
                tick_lbl = (True, True)
            elif col_no == 0:
                tick_lbl = (True, False)
            elif row_no == n_rows - 1:
                tick_lbl = (False, True)
            else:
                tick_lbl = (False, False)
        ax.tick_params(axis='both', which='major', direction='in',
                       labelsize=fontsize,
                       labelbottom=tick_lbl[1], labeltop=False,
                       labelleft=tick_lbl[0], labelright=False)
        if hline is not None:
            ax.axhline(y=hline, color='#008800')
        for k, v in variables.items():
            if k in uncertainties.keys():
                u = uncertainties[k]
                ax.fill_between(time,
                                u[0][:, i],
                                u[1][:, i],
                                color=colours[k],
                                alpha=0.5,
                                linewidth=0.0)
            ax.plot(time, v[:, i], color=colours[k], label=k, linewidth=1.)

    if legend is True:
        ax.legend(loc='best', fontsize=fontsize, fancybox=False)
    elif legend:
        fig.legend(**legend)
    if ofile is not None:
        fig.savefig(ofile, dpi=300)
        plt.close()
    else:
        plt.show()
    return fig


def plot_matshow(
        idata, dim_data,
        plot_dim=[4,3,.1,.3,.15,.05,.05,6], zlim=[None, None], ofile=None
        ):
    r"""Matrix plot.

    Parameters
    ----------
    idata : ndarray
        2d n x n array.
    dim_data : dict
        Dimensionts of original data that forms idata.
        sum([np.prod(v) for v in dim_data.values()]) == n
    plot_dim : list of float    
        width, height of the plot
        left, right, bottom, top plot boundary
        width of colorbar
        fontsize
    zlim: list of None or float
        Colorbar range.
    ofile : path-like
        Output filename.

    """
    fontsize = plot_dim[-1]
    tick_loc = np.zeros(len(dim_data) + 1)
    lbl_loc = np.zeros(len(dim_data))
    idx = 0
    tick_loc[0] = -.5
    for i, (k, v) in enumerate(dim_data.items()):
        n = np.prod(v)
        tick_loc[i+1] = idx + n - .5
        lbl_loc[i] = idx + n / 2 - .5
        idx += n
    fig = plt.figure(figsize=(plot_dim[0], plot_dim[1]), dpi=300)
    ax = fig.add_axes([
        plot_dim[2],
        plot_dim[4],
        1. - plot_dim[2] - plot_dim[3],
        1. - plot_dim[4] - plot_dim[5]
        ])
    matshow = ax.matshow(
            idata, vmin=zlim[0], vmax=zlim[1], cmap='bwr'  #'RdBu_r'
            )

    # ticks
    ax.tick_params(
        axis='both', which='major', direction='out',
        labelsize=fontsize,
        bottom=False, top=False, left=False, right=False,
        #bottom=True, top=False, left=True, right=False,
        labelbottom=False, labeltop=False,
        labelleft=False, labelright=False
        )
    ax.tick_params(
        axis='both', which='minor', direction='out',
        length=0.,
        labelsize=fontsize,
        bottom=False, top=False, left=False, right=False,
        labelbottom=True, labeltop=False,
        labelleft=True, labelright=False
        )
    ax.set_xticks(tick_loc)
    ax.set_xticks(lbl_loc, minor=True)
    ax.set_xticklabels('', major=True)
    ax.set_xticklabels(dim.keys(), minor=True, rotation=90)
    ax.set_yticks(tick_loc)
    ax.set_yticks(lbl_loc, minor=True)
    ax.set_yticklabels('', major=True)
    ax.set_yticklabels(dim.keys(), minor=True)
    ax.grid(
            which='major', alpha=1., color='#000000',
            linestyle='-', linewidth=0.1
            )

    # colorbar
    cax = fig.add_axes([
        1. - plot_dim[3] + 0.01,
        plot_dim[4],
        plot_dim[6],
        1. - plot_dim[4] - plot_dim[5]
        ])
    colorbar = fig.colorbar(
        matshow, cax, orientation='vertical',
        fraction=0.1, pad=0, format='%4.1f',
        )
    colorbar.ax.tick_params(labelsize=fontsize, pad=0.05)
    if ofile is not None:
        fig.savefig(ofile, dpi=300)
        plt.close()
    else:
        plt.show()


def grid_plot(
        idata, locs, tres, time, pargs, params, ofile,
        fig=False, numbering={}, yticks={},
        size=(6, 8), lbl_hemi=False, hlines={}, texts=[], legend=[]
        ):
    r"""Plot data in a grid.

    Parameters
    ----------
    idata : dict
        {variable: {sub_variable: ndarray}}.
        Input data.
    locs : dict
        {variable: (x, y)}.
        Location on the grid.
    tres : dict
        {variable: temporal resolution}.
        The temporal resolution should be present in "time".
    time : dict
        {temporal resolution: ndarray}.
    pargs : dict
        {variable: kwargs}.
        Kwargs for ax.plot.
    params : dict
        {variable: {metadata}}.
        Metadata, including ylim, y-axis label
    ofile : path-like
        Output file.
    numbering : dict
        Optional numbering.
    yticks : dict of list
        Tick locations
    size : list of float
        Figure size.
    lbl_hemi : bool
        Whether to lable hemispheres.
    hlines : dict
        {variable name: list of float}.
    texts : list of dict
        List of kwargs for ax.text.

    """
    if not fig:
        fig = plt.figure(figsize=size, dpi=300)
    fontsize = 6
    fontsize2 = 8

    rowv = max([v[0] for v in locs.values()]) + 1
    cols = max([v[1] for v in locs.values()]) + 1
    boxes = np.zeros((rowv, cols))
    for k, loc in locs.items():
        boxes[loc] = (
            idata[k][list(idata[k].keys())[0]].shape[-1]
            if k in idata and idata[k] else
            1.
            )
    rows = boxes.max(1).sum()
    n_gaps = boxes.max(1) - 1

    marginl = 0.08*6/size[0]
    marginr = 0.01*6/size[0]
    margint = 0.01*8/size[1]
    marginb = 0.02*8/size[1]
    gap = 0.01*8/size[1]
    w = (1 - marginl*cols - marginr) / cols
    h = (1 - margint*rowv - marginb*rowv - n_gaps.sum()*gap) / rows

    if legend:
        legend_objs = {}

    for k, loc in locs.items():  # k = variable
        if k not in idata or not idata[k]:
            continue
        j, i = loc
        t = time[tres[k]]
        xlim = [t[0], t[-1]+t[1]-t[0]]
        ylim = params[k]['ylim']
        if ylim[0] is None:
            ylim[0] = np.nanmin(
                    np.array([np.nanmin(val) for val in idata[k].values()])
                    )
        if ylim[1] is None:
            ylim[1] = np.nanmax(
                    np.array([np.nanmax(val) for val in idata[k].values()])
                    )
        n_prev_plots = boxes[:j].max(1).sum()
        n_prev_gaps = n_gaps[:j].sum()
        n_boxes = int(boxes[loc])
        # Plot location
        xpos = marginl*(i+1) + w*i
        ypos = 1 - margint*(j+1) - marginb*j - n_prev_plots*h - n_prev_gaps*gap
        # Y Label
        fig.text(
                marginl*(i) + w*i + marginr*1.65,
                ypos - (n_boxes*h + (n_boxes-1) * gap) / 2,
                params[k]['lbl'], fontsize=fontsize, rotation=90,
                ha='left', va='center'
                )
        # Figure numbering
        if k in numbering:
            fig.text(
                xpos + marginr, #marginl*(i) + w*i + marginr,
                ypos - 2*gap, #ypos,
                numbering[k], fontsize=fontsize2, rotation=0, fontweight='bold',
                ha='left', va='center'
                )
        # Plots
        for box in range(n_boxes):
            ax = fig.add_axes([
                xpos, ypos - (box+1)*h - gap*box, w, h
                ])
            if box == n_boxes - 1:
                labelbottom = True
            else:
                labelbottom = False
            if not lbl_hemi:
                pass
            elif int(boxes[loc]) == 1:
                ax.annotate(
                    "Global", xy=(0, 1), xycoords="axes fraction",
                    xytext=(2, -2), textcoords="offset points",
                    ha="left", va="top", fontsize=fontsize
                    )
            elif int(boxes[loc]) == 2:
                text = ['30-90\u00B0N', '30-90\u00B0S']
                ax.annotate(
                    text[box], xy=(0, 1), xycoords="axes fraction",
                    xytext=(2, -2), textcoords="offset points",
                    ha="left", va="top", fontsize=fontsize
                    )
            elif int(boxes[loc]) == 4:
                text = [
                        '30-90\u00B0N',
                        '0-30\u00B0N',
                        '0-30\u00B0S',
                        '30-90\u00B0S'
                        ]
                ax.annotate(
                    text[box], xy=(0, 1), xycoords="axes fraction",
                    xytext=(2, -2), textcoords="offset points",
                    ha="left", va="top", fontsize=fontsize
                    )
            ax.set_xlim(*xlim)
            ax.set_ylim(*ylim)
            ax.tick_params(
                axis='both',
                which='major',
                direction='in',
                labelsize=fontsize,
                bottom=True, top=True, left=True, right=True,
                labelbottom=labelbottom, labeltop=False,
                labelleft=True, labelright=False,
                pad=3
                )
            if k in yticks:
                ax.set_yticks(**yticks[k])
            if k in hlines:
                for hline in hlines[k]:
                    ax.axhline(hline, c='#000000', ls='-', lw=0.1)
            for key, val in idata[k].items():  # key = prior/posterior/obs
                if val.shape[0] == 3:
                    ax.fill_between(
                            t, *val[[0, 2], ..., box],
                            facecolor=pargs[key]['c'],
                            alpha=0.3,
                            linewidth=0.0
                            )
            for key, val in idata[k].items():  # key = prior/posterior/obs
                if val.shape[0] == 3:
                    line=ax.plot(
                        t, val[1, ..., box],
                        **pargs[key]
                        )
                elif val.shape[0] == 1:
                    line=ax.plot(
                        t, val[0, ..., box],
                        **pargs[key],
                        )
                if legend and key in legend[0] and key not in legend_objs:
                    legend_objs[key] = line[0]
    for text in texts:
        fig.text(**text, fontsize=fontsize)
    if legend:
        leg = fig.legend(
                handles=[legend_objs[k] for k in legend[0]],
                labels=[i[0] for i in legend[1]],
                **legend[2]
                )
        for n, leg_prop in enumerate(legend[1]):
            if 'linewidth' in leg_prop[1]:
                leg.legendHandles[n].set_linewidth(leg_prop[1]['linewidth'])
            if 'color' in leg_prop[1]:
                leg.legendHandles[n].set_color(leg_prop[1]['color'])
    if ofile:
        fig.savefig(ofile, dpi=300)
    return fig

