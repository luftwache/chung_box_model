# -*- coding: utf-8 -*-
r"""
mcmc_CH4.py

Markov Chain Monte Carlo functions specific for CH4.

The parameters are hardcoded, and hence are not very reusable

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20180710    EC  Initial code.
20200822    EC  Packaged.
"""

# Standard Library imports
import numpy as np
import os
import pandas as pd
import re

# Third party imports
from collections import OrderedDict

# Local imports
import chung_box_model.forward as a_forward
from chung_box_model.io import *
from chung_box_model.chem_CH4 import CH4_table
from chung_box_model.mcmc_utils import *


def set_model_var(
        model, idata, idata_t_res, dates, pslice={}, D_obs=[], D_q={}
        ):
    r"""Set Model variables.

    Parameters
    ----------
    model : Model
        Model to modify.
    idata : dict
        {variable: values}
        Variables and values to modify.
    idata_t_res : dict
        {variable: temporal resolution}
        Temporal resolution of variables.
    dates : dict
        {temporal resolution: DatetimeIndex}
        Dates for each temporal resolution.
    pslice : dict
        {variable: slice}
        Slice parameters if only part of variable is modified.
    D_obs : list
        List of float or empty.
        Ambient clumped isotopic signatures to keep constant to.
        Provide empty list to not force signatures.
    D_q : dict
        {dq_sector: ndarray}
        Source clumped isotopic signatures to keep constant to.

    """
    for k, v in idata.items():
        # yearlies to monthlies
        if idata_t_res[k] not in ['ic', '1M']:
            odata = theta2input(dates['1M'], dates[idata_t_res[k]], v)
        else:
            odata = v.copy()
        # allocate
        if k.startswith(r'q_'):
            if k not in pslice or k not in model.q:
                model.q[k] = odata
            else:
                model.q[k][pslice[k]] = odata
        elif k.startswith(r'dq_'):
            if k not in pslice or k not in model.dq:
                model.dq[k] = include_main_iso(odata, 0.)
            else:
                model.dq[k][..., 1:, :][pslice[k]] = odata
            if k in D_q:
                R = (model.dq[k] * 1.e-3 + 1) * model.R_std
                model.dq[k][:, 3] = (
                        theta2input(
                            dates['1M'], dates[idata_t_res[k]],
                            (D_q[k][:, 0] + 1.e3)
                            )
                        * R[:, 1] * R[:, 2]
                        / model.R_std[3]
                        - 1.e3
                        )
                model.dq[k][:, 4] = (
                        theta2input(
                            dates['1M'], dates[idata_t_res[k]],
                            (D_q[k][:, 1] + 1.e3)
                            )
                        * R[:, 2] * R[:, 2]
                        * 3/8 / model.R_std[4]
                        - 1.e3
                        )
        elif k == 'd0':
            if k not in pslice or model.d0 is None:
                model.d0 = include_main_iso(odata, 0.)
            else:
                model.d0[..., 1:, :][pslice[k]] = odata
            if D_obs:
                R = (model.d0 * 1.e-3 + 1) * model.R_std
                model.d0[3] = (
                        (D_obs[0] + 1.e3) * R[1] * R[2] / model.R_std[3]
                        - 1.e3
                        )
                model.d0[4] = (
                        (D_obs[1] + 1.e3) * R[2] * R[2] * 3/8 / model.R_std[4]
                        - 1.e3
                        )
        elif k in ['KIE_tau']:
            if k not in pslice or model.KIE_tau is None:
                model.KIE_tau = include_main_iso(odata, 1.)
            else:
                model.KIE_tau[..., 1:, :][pslice[k]] = odata
        elif k in ['ib']:
            model.ib = np.tile(odata, (dates['1Y'].size, 1, 1))
        else:
            if k not in pslice or getattr(model, k) is not None:
                setattr(model, k, odata)
            else:
                getattr(model, k)[pslice[k]] = odata
    if [k for k in ['tv', 'vv'] if k in idata]:
        model.ib = np.tile(
                a_forward.transport_matrix(
                    model.ti, model.vi, model.tv, model.vv
                    ),
                (dates['1Y'].size, 1, 1)
                )


def model_lnlike(
        theta, model, dates, ptres,
        p1, p2, p3,
        y_var, theta_var, dim_theta,
        pslice={}, D_obs=[], D_q={}, iso_sel=[]
        ):
    r"""Model ln likelihood

    Parameters
    ----------
    theta : ndarray
        1d.
        State vector.
    model : chung_box_model.model.Model
        Model object with unperturbed data prescribed.
    dates : dict
        Datetime vector. {date_key: pd.date_range}
    ptres : dict
        Date parameters. {key: date key}
    p1, p2, p3 : dict
        Distribution parameters. {key: ndarray/str}
                        p1      p2          p3
            normal      mean    std dev     'n'
            uniform     lower   upper bound 'u'
            m. normal   mean    covaricance 'n2'
    dim_theta : dict
        Dimesions of theta. {key: tuple-like}
    pslice : dict, optional
        {key: list of int}
        Isotopologues to update. [..., [value], :] gets updated
    D_obs : ndarray
        [D_13CH3D, D_12CH2D2]
    D_q : dict
        {key: [D_13CH3D, D_12CH2D2]}
    iso_sel : list
        Selection of isotopologues.

    Returns
    -------
    L : float
        Log likelihood
    y_out : ndarray
        1d.
        Observation vector with misc data at the end.

    """
    np.seterr(all='ignore')
    # =========================================================================
    #     Convert theta to input values
    # =========================================================================
    theta_p = expand_arrays(theta, dim_theta)
    L_theta = sum([
        ln_like(x=theta_p[k], p1=p1[k], p2=p2[k], pdf=p3[k])
        for k in theta_var
        ])
    set_model_var(
            model, theta_p, ptres, dates, pslice, D_obs, D_q
            )

    # =========================================================================
    #     Run model
    # =========================================================================
    odata = model.run(iso_sel=iso_sel)

    # y_model
    y_m = OrderedDict()
    c = odata['c'][..., 0:4]
    idx = {iso: i for i, iso in enumerate(model.iso)}
    i0 = idx['C12H4']
    if 'c' in y_var:
        if not iso_sel:
            y_m['c'] = c.sum(axis=-2)
        else:
            y_m['c'] = c[..., iso_sel, :].sum(axis=-2)
    if 'd_13C' in y_var:
        i1 = idx['C13H4']
        y_m['d_13C'] = (c[:, i1]/c[:, i0]/model.R_std[i1] - 1)*1.e3
    if 'd_2H' in y_var:
        i2 = idx['C12H3D']
        y_m['d_2H'] = (c[:, i2]/c[:, i0]/model.R_std[i2] - 1)*1.e3
    if 'D_C13H3D' in y_var:
        i1 = idx['C13H4']
        i2 = idx['C12H3D']
        i3 = idx['C13H3D']
        y_m['D_C13H3D'] = (c[:, i3]*c[:, i0]/c[:, i1]/c[:, i2] - 1)*1.e3
    if 'D_C12H2D2' in y_var:
        i2 = idx['C12H3D']
        i4 = idx['C12H2D2']
        y_m['D_C12H2D2'] = (c[:, i4]*c[:, i0]/c[:, i2]/c[:, i2]*8/3 - 1)*1.e3

    L_y = sum([
        ln_like(x=y_m[k], p1=p1[k], p2=p2[k], pdf=p3[k])
        for k in y_var
        ])
    L = L_theta + L_y
    return L, c


def calc_y(y_var, isos, c, iso_sel=[]):
    r"""Calculate y vector from mixing ratios

    Parameters
    ----------
    y_var : list of str
        Variables in y vector.
    isos : list of str
        Isotopologues included in mixing ratio data in correct order.
    c : path-like
        Mixing ratios.
    iso_sel : list of int
        Boxes to exclude when caculating sum of 'c'.

    Returns
    -------
    y_out : dict
        Y vector.

    """
    y_out = OrderedDict()
    idx = {iso: i for i, iso in enumerate(isos)}
    # i"n" may differ from that of isos
    #     i0 - C12H4
    #     i1 - C13H4
    #     i2 - C12H3D
    #     i3 - C13H3D
    #     i4 - C12H2D2
    i0 = idx['C12H4']
    if 'c' in y_var:
        if not iso_sel:
            y_out['c'] = c.sum(axis=-2)
        else:
            y_out['c'] = c[..., iso_sel, :].sum(axis=-2)
    if 'd_13C' in y_var:
        i1 = idx['C13H4']
        y_out['d_13C'] = \
            (c[..., i1, :] / c[..., i0, :] /
             CH4_table.R_std['C13H4'] - 1.) * 1.e3
    if 'd_2H' in y_var:
        i2 = idx['C12H3D']
        y_out['d_2H'] = \
            (c[..., i2, :] / c[..., i0, :] /
             CH4_table['R_std']['C12H3D'] - 1.) * 1.e3
    if 'D_C13H3D' in y_var:
        i1 = idx['C13H4']
        i2 = idx['C12H3D']
        i3 = idx['C13H3D']
        y_out['D_C13H3D'] = \
            (c[..., i3, :] * c[..., i0, :] /
             c[..., i1, :] / c[..., i2, :] - 1.) * 1.e3
    if 'D_C12H2D2' in y_var:
        i2 = idx['C12H3D']
        i4 = idx['C12H2D2']
        y_out['D_C12H2D2'] = \
            (c[..., i4, :] * c[..., i0, :] /
             c[..., i2, :] / c[..., i2, :] * 8./3. - 1) * 1.e3
    return y_out


def get_vars(years, base_dir):
    r"""Get observation and prior data

    Parameters
    ----------
    years : list
        [start, end] in strings
        Start and End year
    base_dir : path-like
        Location of the base directory containing prior data.

    Returns
    -------
    p1, p2, p3 : dict
        Distribution parameters. {key: ndarray}
                        p1      p2          p3
            normal      mean    std dev     'n'
            uniform     lower   upper bound 'u'
    ptres : dict
        Temporal resolution of data.
    pdate : dict
        Date index for each temporal resolution.
    pmeta : dict
        Metadata.

    """
    p1 = OrderedDict()
    p2 = OrderedDict()
    p3 = OrderedDict()
    ptres = OrderedDict()
    pdate = OrderedDict()
    pmeta = OrderedDict()

    years_d = [pd.to_datetime(str(i)) for i in years]
    dates1M = pd.date_range(years_d[0], years_d[1], freq='1MS', closed='left')
    dates1Y = pd.date_range(years_d[0], years_d[1], freq='1YS', closed='left')
    dates5Y = pd.date_range(years_d[0], years_d[1], freq='5YS', closed='left')
    dates_base = pd.date_range('1680', '2020', freq='1YS', closed='left')
    # dates_base = pd.DatetimeIndex([str(i) for i in range(1680, 1740, 30)] +
    #                               [str(i) for i in range(1740, 1860, 20)] +
    #                               [str(i) for i in range(1860, 1970, 10)] +
    #                               [str(i) for i in range(1970, 2020, 1)])
    # dates1V = dates_base[(dates1Y >= str(years[0])) &
    #                      (dates1Y < str(years[1]))]
    date0 = dates1M[0] - pd.DateOffset(months=1)
    pdate['1M'] = dates1M
    pdate['1Y'] = dates1Y
    pdate['5Y'] = dates5Y
    #pdate['1V'] = dates1V
    #pdate['dates_y'] = dates1M
    #pdate['dates_theta'] = dates1Y
    n_years = years[1] - years[0]
    boxes = ['box_{}'.format(i) for i in range(4)]

    # =========================================================================
    #     uncertainties
    # =========================================================================
    unc = {
        'm_atm': [0.01, 'u', 'm'],
        'c': {'cmip6': [0.02, 'n', 'm'],
              'agage': [0.01, 'n', 'm']},
        'c0': [.1, 'u', 'm'],
        #'d_13C': [0.005, 'n', 'm'],
        'd_13C': [0.2, 'n', 'a'],  # precision = 0.1 per mil
        #'d_2H': [0.03, 'n', 'm'],
        'd_2H': [4., 'n', 'a'],  # precision = 2 per mil
        'D_C13H3D': [0.3, 'n', 'a'],
        'D_C12H2D2': [0.3, 'n', 'a'],
        'd0': [0.05, 'u', 'm'],
        'q': [0.3, 'n', 'm'],
        'dq': [0.1, 'n', 'm'],
        'OH': [0.2, 'n', 'm'],
        'OH_a': [0.2, 'n', 'a'],
        'Cl': [0.2, 'n', 'm'],
        'Cl_a': [0.5, 'n', 'a'],
        'T': [0.2, 'n', 'm'],
        'tau': "h",
        'KIE_tau': "h",
        'ib': "h"
        }

    # =========================================================================
    #     Temporal resolution
    # =========================================================================
    t_res = {
        'm_atm': 'ic',
        'c': '1M',
        'c0': 'ic',
        'd_13C': '1M',
        'd_2H': '1M',
        'D_C13H3D': '1M',
        'D_C12H2D2': '1M',
        'd0': 'ic',
        'q': '1Y',
        'dq': '1Y',
        'OH': '1M',
        'OH_a': '1Y',
        'Cl': '1M',
        'Cl_a': '1Y',
        'T': '1Y',
        'tau': '1Y',
        'KIE_tau': 'ic',
        'ib': '1Y'
        }
    # =========================================================================
    #     mass (g) - mass of atm in each boxes
    # =========================================================================
    m_atm = 5.1352e21  # mass of dry air (Trenberth, 2005)
    mass = np.repeat([0.5, 0.3, 0.2], 4)/4.0*m_atm
    p1['m_atm'], p2['m_atm'] = calc_pdf_params(mass, *unc['m_atm'])
    p3['m_atm'] = unc['m_atm'][1]
    ptres['m_atm'] = t_res['m_atm']

    # =========================================================================
    #     c (ppb) - obs mixing ratio
    # =========================================================================
    key = 'c'
    idata = pd.DataFrame(np.nan, index=pdate[t_res[key]], columns=boxes)
    idata0a = pd.DataFrame(np.nan, index=[date0], columns=boxes)
    idata_s = pd.DataFrame(np.nan, index=pdate[t_res[key]], columns=boxes)
    idata0a_s = pd.DataFrame(np.nan, index=[date0], columns=boxes)
    # CMIP6 input
    c = pd.read_hdf(os.path.join(base_dir, 'CH4/obs_cmip6_input.hdf'), 'CH4'
                    ) / 1.0003
    idata.update(c)
    idata0a.update(c)
    idata_s.update(c*unc[key]['cmip6'][0])
    idata0a_s.update(c*unc[key]['cmip6'][0])
    # AGAGE
    c = pd.read_hdf(os.path.join(base_dir, 'CH4/obs_agage.hdf'), 'CH4')/1.e3
    idata.update(c)
    idata0a.update(c)
    idata_s.update(c*unc[key]['agage'][0])
    idata0a_s.update(c*unc[key]['agage'][0])
    # Allocate
    p1[key] = np.array(idata)
    p2[key] = np.array(idata_s)
    p3[key] = 'n'
    ptres[key] = t_res[key]

    # =========================================================================
    #     c0 (ppb) - initial mixing ratio
    # =========================================================================
    key = 'c0'
    if years[0] < 1970:
        unc[key][0] *= 2
    idata0 = pd.DataFrame(
                np.atleast_2d([idata[i].loc[idata[i].first_valid_index()]
                               for i in idata]),
                index=[date0], columns=boxes)
    idata0_s = idata0 * unc[key][0]
    idata0.update(idata0a)
    idata0_s.update(idata0a_s)
    p1[key] = np.zeros((1, 12))
    p2[key] = np.zeros((1, 12))
    p3[key] = unc[key][1]
    if p3[key] == 'n':
        p1[key][:, 0:4] = np.array(idata0)
        p1[key][:, 4:12] = np.tile(p1[key][:, 0:4].mean(), (1, 8))
        p2[key][:, 0:4] = np.array(idata0_s)
        p2[key][:, 4:12] = p1[key][:, 4:12]*unc[key][0]
    elif p3[key] == 'u':
        p1[key][:, 0:4] = np.array(idata0) - np.array(idata0_s)
        p2[key][:, 0:4] = np.array(idata0) + np.array(idata0_s)
        p1[key][:, 4:12] = np.array(idata0).mean() * (1 - unc[key][0])
        p2[key][:, 4:12] = np.array(idata0).mean() * (1 + unc[key][0])
    ptres[key] = t_res[key]

    # =========================================================================
    #     d_13C (per mil) - obs delta
    # =========================================================================
    key = 'd_13C'
    d_13C = (
        pd.concat(
            (pd.read_hdf(os.path.join(base_dir, 'CH4/obs_instaar.hdf'),
                         'd_13C'),
             pd.read_hdf(os.path.join(base_dir, 'CH4/obs_caos.hdf'),
                         'd_13C')
             ),
            axis=1
            ).groupby(level=0, axis=1).mean()
        )
    idata = pd.DataFrame(np.nan, index=pdate[t_res[key]], columns=c.columns)
    idata.update(d_13C)
    p1[key], p2[key] = calc_pdf_params(np.array(idata), *unc[key])
    p3[key] = unc[key][1]
    ptres[key] = t_res[key]

    # =========================================================================
    #     d_2H (per mil) - obs delta
    #       Assuming random distribution
    #           R_sample and R_std shares same factor, hence
    #           dD = d12CH3D
    # =========================================================================
    key = 'd_2H'
    d_2H = (
        pd.concat(
            (pd.read_hdf(os.path.join(base_dir, 'CH4/obs_instaar.hdf'),
                         'd_2H'),
             pd.read_hdf(os.path.join(base_dir, 'CH4/obs_caos.hdf'),
                         'd_2H')
             ),
            axis=1
            ).groupby(level=0, axis=1).mean()
        )
    #d_2H = pd.read_hdf(os.path.join(base_dir, 'CH4/obs_instaar.hdf'), 'd_2H')
    idata = pd.DataFrame(np.nan, index=pdate[t_res[key]], columns=c.columns)
    idata.update(d_2H)
    p1[key], p2[key] = calc_pdf_params(np.array(idata), *unc[key])
    p3[key] = unc[key][1]
    ptres[key] = t_res[key]

    # =========================================================================
    #     D_C13H3D (per mil) - obs delta
    # =========================================================================
    idata = pd.DataFrame(np.nan, index=pdate[t_res[key]], columns=c.columns)
    idata.loc['2000':] = 3.75
    p1['D_C13H3D'], p2['D_C13H3D'] = calc_pdf_params(np.array(idata),
                                                     *unc['D_C13H3D'])
    p3['D_C13H3D'] = unc['D_C13H3D'][1]
    ptres['D_C13H3D'] = '1M'

    # =========================================================================
    #     D_C12H2D2 (per mil) - obs delta
    # =========================================================================
    idata = pd.DataFrame(np.nan, index=pdate[t_res[key]], columns=c.columns)
    idata.loc['2000':] = 100.0
    p1['D_C12H2D2'], p2['D_C12H2D2'] = calc_pdf_params(np.array(idata),
                                                       *unc['D_C12H2D2'])
    p3['D_C12H2D2'] = unc['D_C12H2D2'][1]
    ptres['D_C12H2D2'] = '1M'

    # =========================================================================
    #     d0 - (per mil) initial condition delta
    # =========================================================================
    d0 = np.zeros((5, 12))
    if years[0] < 1970:
        sigma = unc['d0'][0] * 2
    else:
        sigma = unc['d0'][0]

    d_13Ci = [d_13C[i][date0:].loc[d_13C[i][date0:].first_valid_index()]
              for i in d_13C]
    d0[1, 0:4] = np.array(d_13Ci)

    d_2Hi = [d_2H[i][date0:][d_2H[i][date0:].first_valid_index()]
             for i in d_2H]
    d0[2, 0:4] = np.array(d_2Hi)

    R_std = np.array(
                CH4_table['R_std'][['C12H4', 'C13H4', 'C12H3D']]
                ).reshape(3, 1)

    R0 = (d0[0:3, 0:4] * 1e-3 + 1.) * R_std
    ci0 = (p1['c0'][: , 0:4] * R0 / R0.sum(0)).sum(1, keepdims=True)
    d0[:3, 4:12] = (ci0/ci0[0]/R_std - 1.) * 1.e3

    # Big delta = 0.0
    R_C13H4 = (d0[1]/1.e3 + 1.0) * CH4_table['R_std']['C13H4']
    R_C12H3D = (d0[2]/1.e3 + 1.0) * CH4_table['R_std']['C12H3D']
    d0[3] = \
        (R_C13H4*R_C12H3D/CH4_table['R_std']['C13H3D'] - 1.0) * 1.e3
    d0[4] = \
        (3.0/8.0*R_C12H3D**2/CH4_table['R_std']['C12H2D2'] - 1.0) * 1.e3

    p3['d0'] = unc['d0'][1]
    if p3['d0'] == 'n':
        p1['d0'] = np.array(d0)[..., 1:, :]
        p2['d0'] = np.zeros(p1['d0'].shape)
        p2['d0'][:, :4] = np.absolute(p1['d0'][:, :4])*sigma
        p2['d0'][:, 4:] = np.absolute(p1['d0'][:, 4:])*sigma*2
    else:
        p1['d0'] = np.zeros((4, 12))
        p2['d0'] = np.zeros(p1['d0'].shape)
        p1['d0'][:, :4] = d0[1:, :4] - np.abs(d0[1:, :4])*sigma
        p2['d0'][:, :4] = d0[1:, :4] + np.abs(d0[1:, :4])*sigma
        p1['d0'][:, 4:] = d0[1:, 4:] - np.abs(d0[1:, 4:])*sigma*2
        p2['d0'][:, 4:] = d0[1:, 4:] + np.abs(d0[1:, 4:])*sigma*2
    ptres['d0'] = 'ic'

    # =========================================================================
    #     dq (per mil) - emission delta
    #       order = ['C12H4', 'C13H4', 'C12H3D', 'C12H2D2', 'C13H3D']
    # =========================================================================
    dist = 'u'
    sigma1 = 0.1
    dq = {'dq_total': [0.0, -54.2, -295.0, -330.4, -492.8],
          'dq_ftot': [0.0, -41.9, -177.3, -209.4, -317.5],
          'dq_mant': [0.0, -60.7, -321.7, -360.6, -535.0],
          'dq_mnat': [0.0, -58.1, -338.1, -375.3, -551.8],
          'dq_btot': [0.0, -24.6, -225.0, -241.8, -394.8]}
    # dq = {'dq_total': [0.0, -54.2, -295.0, -330.4, -492.8],
    #       'dq_ftot': [0.0, -40.9, -162.5, -194.3, -292.9],
    #       'dq_mant': [0.0, -60.7, -321.7, -360.6, -535.0],
    #       'dq_mnat': [0.0, -58.2, -334.9, -372.4, -547.3],
    #       'dq_btot': [0.0, -24.6, -225.0, -241.8, -394.8]}
    for k, v in dq.items():
        p1[k], p2[k] = calc_pdf_params(
                np.array(v).reshape(1, -1, 1)[..., 1:, :],
                *unc['dq'])
        p3[k] = unc['dq'][1]
        ptres[k] = 'ic'

    # =========================================================================
    #     q (Tg) - emissions
    # =========================================================================
    key = 'q'
    q_sectors = {}
    method = 'linear'#'time'

    # Distribution based on
    #     Jet Propulsion Laboratory. 2013. ISLSCP II Land and Water Masks with
    #     Ancillary Data. ORNL DAAC, Oak Ridge, Tennessee, USA.
    #     https://doi.org/10.3334/ORNLDAAC/1200
    # Assumes sources are equally spread across the area.
    dist_land = np.array([.43, .24, .20, .13])
    dist_ocean = np.array([.18, .25, .27, .30])

    # EDGAR (Gg per gridcell per year, from yearly)
    # 1. Power/Transport; 2. Heavy Industry; 4. Fermentation; 6. Waste Water
    # 7. Fossil Fuel Fire (exposed coal)
    meta = {'q_ftot': ['cat1', 'cat2', 'cat7'],
            'q_mant': ['cat4', 'cat6']}

    prior_file = os.path.join(base_dir, 'CH4/prior_edgar_1970_2012.hdf')
    for k, v in meta.items():
        q_sectors[k] = pd.DataFrame(np.nan, index=dates_base, columns=boxes)
        idata = sum([pd.read_hdf(prior_file, v) for v in v])
        q_sectors[k].update(idata)
        q_sectors[k].ffill(inplace=True)
        # q_sectors[k].bfill(inplace=True)

    # Interpolate fossil fuel emission between 1680 and 1970 (pre-1680 = 0)
    q_sectors['q_ftot'][:'1680'] = 0.
    q_sectors['q_ftot'].interpolate(method=method, inplace=True)

    # Interpolate mant (20 %-> 100 % of 1970 from World bank demography)
    q_sectors['q_mant']['1680'] = q_sectors['q_mant']['1970'].values * 0.2
    q_sectors['q_mant'].interpolate(method=method, inplace=True)

    # WetCHARTs (Gg per gridcell per year, from monthly)
    prior_file = os.path.join(base_dir, 'CH4/prior_wetcharts_2001_2015.hdf')
    q_sectors['q_mnat'] = pd.DataFrame(np.nan, index=dates_base, columns=boxes)
    idata = pd.read_hdf(prior_file, 'CH4')
    q_sectors['q_mnat'].update(idata.resample('YS').mean())
    q_sectors['q_mnat'].ffill(inplace=True)
    q_sectors['q_mnat'].bfill(inplace=True)

    # Flat oceanic, freshwater and termites to q_mnat
    # q_sectors['q_mnat'] += 30.e3 / 4.
    # Flat geological seepage (Schwietzke et al., 2017)
    # q_sectors['q_ftot'] += 50.e3 / 4.

    # Flat oceanic (Saunois, 2016) (land/ocean distribution)
    q_sectors['q_mnat'] += 2.e3 * dist_ocean
    # Flat termite (Saunois, 2016) (guessed distribution)
    q_sectors['q_mnat'] += 9.e3 * np.array([.10, .40, .45, .05])
    # Flat geological seepage (Saunois, 2016) (land/ocean distribution)
    q_sectors['q_ftot'] += 40.e3 * dist_land
    q_sectors['q_ftot'] += 12.e3 * dist_ocean

    # GFED (Gg per gridcell per year, from monthly)
    prior_file = os.path.join(base_dir, 'CH4/prior_gfed_1997_2016.hdf')
    q_sectors['q_btot'] = pd.DataFrame(np.nan, index=dates_base, columns=boxes)
    idata = pd.read_hdf(prior_file, 'CH4')
    q_sectors['q_btot'].update(idata.resample('YS').mean())
    q_sectors['q_btot'].ffill(inplace=True)
    # q_sectors['q_btot'].bfill(inplace=True)
    q_sectors['q_btot'][:'1996'] = \
        np.tile(np.mean(q_sectors['q_btot']['1997':'2001'], axis=0),
                (q_sectors['q_btot'][:'1996'].shape[0], 1))

    for k, v in q_sectors.items():
        # Gg -> Tg
        p1[k], p2[k] = calc_pdf_params(
            np.array(v.loc[pdate[t_res[key]]]/1.e3)[..., np.newaxis, :],
            *unc[key])
        p3[k] = unc[key][1]
        ptres[k] = '1Y'

    p1['q_total'] = sum([p1[k] for k in q_sectors.keys()])
    p2['q_total'] = sum([p2[k] for k in q_sectors.keys()])
    p3['q_total'] = unc[key][1]
    ptres['q_total'] = t_res[key]

    # =========================================================================
    #     OH - [OH]
    # =========================================================================
    key = 'OH'
    prior_file = os.path.join(base_dir, 'OH.npy')
    p1[key], p2[key] = calc_pdf_params(
            np.tile(r_npy(prior_file)/1.e6, (n_years, 1)),
            *unc[key])
    p3[key] = unc[key][1]
    ptres[key] = t_res[key]

    # =========================================================================
    #     Cl - [Cl]
    # =========================================================================
    prior_file = os.path.join(base_dir, 'Cl.npy')
    p1['Cl'], p2['Cl'] = calc_pdf_params(
            np.tile(r_npy(prior_file)/1.e6, (n_years, 1)),
            *unc['Cl'])
    p3['Cl'] = unc['Cl'][1]
    ptres['Cl'] = '1M'

    # =========================================================================
    #     OH_a - [OH] anomaly
    # =========================================================================
    p1['OH_a'], p2['OH_a'] = calc_pdf_params(
            np.zeros((dates1Y.size, 1)),
            *unc['OH_a'])
    p3['OH_a'] = unc['OH_a'][1]
    ptres['OH_a'] = '1Y'

    # =========================================================================
    #     Cl_a - [Cl] anomaly
    # =========================================================================
    p1['Cl_a'], p2['Cl_a'] = calc_pdf_params(
            np.zeros((dates1Y.size, 1)),
            *unc['Cl_a'])
    p3['Cl_a'] = unc['Cl_a'][1]
    ptres['Cl_a'] = '1Y'

    # =========================================================================
    #     T - temperature
    # =========================================================================
    prior_file = os.path.join(base_dir, 'temperature.npy')
    p1['T'], p2['T'] = calc_pdf_params(
            np.tile(r_npy(prior_file), (n_years, 1)),
            *unc['T'])
    p3['T'] = unc['T'][1]
    ptres['T'] = '1M'

    # =========================================================================
    #     tau - lifetime
    #           soil and tropospheric Cl sink from Saunois et al., 2016
    #           stratospheic lifetime from Chipperfield et al., 2013
    #           assumes Cl loss is uniformly distributed in the troposphere
    # =========================================================================
    tau = np.full((len(dates1Y), 3, 1, 12), np.nan)
    burden = 1800 * 1.e-9 * mass * 1.e-12 * 16.043 / 28.97
    loss_cl_t = 25.
    loss_soil = 28.
    tau_strat = 159.6

    tau[:, 0, :, :4] = burden[:4] / (5./8. * loss_cl_t / 4.)
    tau[:, 0, :, 4:8] = burden[4:8] / (3./8. * loss_cl_t / 4.)
    tau[:, 1, :, :4] = burden[:4] / (loss_soil*dist_land)
    tau[:, 2, :, 8:] = tau_strat*.2

    p1['tau'] = np.reciprocal(np.nansum(np.reciprocal(tau), 1, keepdims=True))
    #p1['tau'] = tau
    p2['tau'] = p1['tau']*.2
    p3['tau'] = 'n'
    ptres['tau'] = '1Y'

    # =========================================================================
    #     KIE tau - lifetime KIE
    #       Surface and stratosphere -> 1.0 - KIE_Cl
    #       Upper troposphere -> 0.9-1.1*(KIE_Cl - 1) + 1
    #       Soil and O(1D) have very low KIE compared to KIE_Cl
    #       KIE_soil from Snover and Quay (2001)
    #       KIE_strat is Roeckmann et al. (2011)
    #       Method from Whitehill (2017) used for clumped methane
    # =========================================================================
    isos = ['C13H4', 'C12H3D', 'C13H3D', 'C12H2D2']
    KIE_soil = [(1.0173+1.0181)/2, (1.099+1.066)/2]
    KIE_strat = np.array([
        1.0127, 1.137
        # [(1.0159+1.0156)/2, 1.0127],
        # [(1.138+1.151)/2, 1.122]
        ])

    KIE = np.zeros((3, 4, 1))
    KIE[0, 0] = CH4_table['KIE_Cl']['C13H4']
    KIE[0, 1] = CH4_table['KIE_Cl']['C12H3D']
    KIE[0, 2] = CH4_table['KIE_Cl']['C13H3D']
    KIE[0, 3] = CH4_table['KIE_Cl']['C12H2D2']

    KIE[1, 0] = KIE_soil[0]
    KIE[1, 1] = KIE_soil[1]
    KIE[1, 2] = KIE_soil[0] * KIE_soil[1]
    KIE[1, 3] = KIE_soil[1] * KIE_soil[1]

    KIE[2, 0] = KIE_strat[0]
    KIE[2, 1] = KIE_strat[1]
    KIE[2, 2] = KIE_strat[0] * KIE_strat[1]
    KIE[2, 3] = KIE_strat[1] * KIE_strat[1]

    tau1 = np.reciprocal(
            np.nansum(
                np.reciprocal(tau[0] * include_main_iso(KIE, 1.)),
                axis=0, keepdims=True)
            )
    #tau1 = CH4_table['KIE_Cl'][isos].reshape(4: 1)
    p1['KIE_tau'] = (tau1/tau1[..., :1, :])[..., 1:, :]
    p2['KIE_tau'] = p1['KIE_tau']*.2

    '''
    p1['KIE_tau'] = KIE
    p2['KIE_tau'] = KIE.copy()

    p2['KIE_tau'][0] = .01

    p2['KIE_tau'][1, 0] = 0.5 * (0.0010**2 + 0.0004**2)**.5
    p2['KIE_tau'][1, 1] = 0.5 * (0.030**2 + 0.007**2)**.5
    p2['KIE_tau'][1, 2] = (
            (
                (p2['KIE_tau'][1, 0] / KIE[1, 0])**2
                + (p2['KIE_tau'][1, 1] / KIE[1, 1])**2
                )**.5
            * KIE[1, 2]
            )
    p2['KIE_tau'][1, 3] = p2['KIE_tau'][1, 1] * 2 / KIE[1, 1] * KIE[1, 3]

    p2['KIE_tau'][2, 0] = 0.0002
    p2['KIE_tau'][2, 1] = 0.014
    p2['KIE_tau'][2, 2] = (
            (
                (p2['KIE_tau'][2, 0] / KIE[2, 0])**2
                + (p2['KIE_tau'][2, 1] / KIE[2, 1])**2
                )**.5
            * KIE[2, 2]
            )
    p2['KIE_tau'][2, 3] = p2['KIE_tau'][2, 1] * 2 / KIE[2, 1] * KIE[2, 3]
    '''

    p3['KIE_tau'] = 'n'
    ptres['KIE_tau'] = 'ic'

    # =========================================================================
    #     KIE_tau_factor - Multiplicative factor for KIE_tau
    #       Providing alternative way to adjust KIE_tau
    #       Works as:
    #           KIE_tau (final) = (KIE_tau - 1) * KIE_tau_factor + 1
    #       This ensures KIE always increases as M_CH4 increases and above 1
    # =========================================================================
    '''
    p1['KIE_tau_factor'] = np.ones((3, 1, 1))
    p2['KIE_tau_factor'] = np.ones((3, 1, 1))
    p2['KIE_tau_factor'][0] = 0.02
    p2['KIE_tau_factor'][1] = 0.2
    p2['KIE_tau_factor'][2] = 0.1
    '''
    p1['KIE_tau_factor'] = np.ones((1, 1, 1))
    p2['KIE_tau_factor'] = np.full((1, 1, 1), 0.15)
    p3['KIE_tau_factor'] = 'n'
    ptres['KIE_tau_factor'] = 'ic'

    # =========================================================================
    #     ib - Inter-box transport
    # =========================================================================
    prior_file = os.path.join(base_dir, 'transport.npz')
    transport_i_t, transport_i_v1, transport_t, transport_v1 = \
        r_npz(prior_file)
    transport_t *= 86400.0
    transport_v1 *= 86400.0

    pmeta['ti'] = transport_i_t
    pmeta['vi'] = transport_i_v1
    
    p1['tv'] = transport_t
    p1['vv'] = transport_v1
    for k in ['tv', 'vv']:
        p2[k] = p1[k]*0.2
        p3[k] = 'n'
        ptres[k] = 'ic'

    """
    ib = np.zeros((12, 12, 12))
    for mi in range(12):
        ib[mi] = a_forward.transport_single(
            i_t=transport_i_t,
            i_v1=transport_i_v1,
            t_in=transport_t[mi],
            v1_in=transport_v1[mi]
            )
    """
    ib = a_forward.transport_matrix(
        i_t=transport_i_t,
        i_v1=transport_i_v1,
        t_in=transport_t,
        v1_in=transport_v1
        )
    p1['ib'] = ib
#    p1['ib'] = np.tile(ib, (n_years, 1, 1))
#    p1['ib'] = np.tile(np.mean(ib, axis=0), (dates1Y.size*12, 1, 1))
    p2['ib'] = p1['ib']*0.2
    p3['ib'] = 'n'
#    ptres['ib'] = '1M'
    ptres['ib'] = 'ic'

    # =========================================================================
    #     dummy - dummy value to check distribution
    # =========================================================================
    p1['dummy'] = np.zeros((dates1Y.size, 1))
    p2['dummy'] = np.full((dates1Y.size, 1), 10.0)
    p3['dummy'] = 'n'
    ptres['dummy'] = '1Y'

    return p1, p2, p3, ptres, pdate, pmeta

